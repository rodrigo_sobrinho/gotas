var tics;
var FPS = 30;
var intervalId;
var spawnIntervalId;

var container;

var gotas = {};
var idCounter = 0;

function main(){
	tics = Math.ceil(1000 / FPS);
	initGame();
	intervalId = window.setInterval(updateGame, tics);
	spawnIntervalId = window.setInterval(spawnGota, 2000);
}

function initGame(){

	container = new Palco("gameBoard");
}

function updateGame(){

	$.each(gotas, function(i,v){

		v.update();
	});
}

function spawnGota(){

	gotas[idCounter] = new Gota({
		id: idCounter,
		palco: container
	});

	idCounter++;

	container.background.toFront();
}

$(document).ready(function(){
	main();
});

Palco = function(palco_id, args){

	if(typeof args == "undefined"){
		args = {};
	}

	this.palco_id = palco_id;
	this.container = null;
	this.x = 0;
	this.y = 0;
	this.width = $("#"+palco_id).width();
	this.height = $("#"+palco_id).height();

	this.background = null;
	this.copo = null;
	this.score_text = null;
	this.sound = new Howl({
		src: ['sound/water-droplet-1.wav']
    });

	this.score = 0;

	this.init = function(){

		this.container = Raphael(palco_id, this.width, this.height);

		this.background = this.container.rect(this.x, this.y, this.width, this.height)
			.attr({
				'fill': 'transparent'
			})
			.mousemove(function(e){
				
				var self = this.parent;

				self.copo.move(e.layerX);
			});
		this.background.parent = this;

		this.copo = new Copo({
			palco: this
		});

		this.score_text = this.container.text(this.width - 10, this.y + 15, this.score).attr({
			'text-anchor': 'end',
			'font-size': '20px'
		});

		this.background.toFront();

		return this;
	}

	this.init();

	return this;
}

Copo = function(args){

	if(typeof args == "undefined"){
		args = {};
	}

	this.palco = args.palco || null;
	this.x = 300;
	this.y = 430;
	this.width = 26;
	this.height = 50;
	this.forma = null;

	this.init = function(){

		this.forma = this.palco.container.set();

		this.forma.push(this.palco.container.image("img/copo.png", this.x - (this.width / 2), this.y - (this.height / 2), this.width, this.height));

		return this;
	}

	this.move = function(x){

		if(x > 575)
			x = 575;

		if(x < 25)
			x = 25;

		this.x = x;
		this.forma.attr({
			x: this.x - (this.width / 2)
		});
	}

	this.init();

	return this;
}

Gota = function(args){

	if(typeof args == "undefined"){
		args = {};
	}

	this.id = args.id;
	this.palco = args.palco || null;
	this.x = null;
	this.y = null;
	this.width = 18;
	this.height = 30;
	this.forma = null;

	this.init = function(){

		this.forma = this.palco.container.set();

		this.x = Math.ceil(Math.random() * 600);
		this.y = -30;

		if(this.x < 25)
			this.x = 25;

		if(this.x > 475)
			this.x = 475;

		this.forma.push(this.palco.container.image("img/gota.png", this.x - (this.width / 2), this.y - (this.height / 2), this.width, this.height));

		return this;
	}

	this.update = function(){

		this.y += 4;
		this.forma.attr({
			y: this.y
		});

		if(this.y > 550){

			this.remove();
		}

		var pontoX = this.x;
		var pontoY = this.y + (this.height / 2) - 5 + (this.width / 2);

		if(pontoY > 400){

			if(pontoX >= this.palco.copo.x - (this.palco.copo.width / 2) && pontoX <= (this.palco.copo.x - (this.palco.copo.width / 2) + this.palco.copo.width) && pontoY >= this.palco.copo.y - (this.palco.copo.height / 2) && pontoY <= this.palco.copo.y - (this.palco.copo.height / 2) + 20){

				this.remove();
				this.palco.score++;
				this.palco.score_text.attr({
					text: this.palco.score
				});
				this.palco.sound.play();
			}
		}
	}

	this.remove = function(){

		this.forma.remove();
		delete gotas[this.id];
	}

	this.init();

	return this;
}